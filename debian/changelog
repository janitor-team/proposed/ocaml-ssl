ocaml-ssl (0.5.10-1) unstable; urgency=low

  * New upstream version 0.5.10
  * Bump standards-version to 4.5.1 (no change)
  * Install upstream changelog

 -- Kyle Robbertze <paddatrapper@debian.org>  Sun, 15 Aug 2021 08:53:03 +0200

ocaml-ssl (0.5.9-2) unstable; urgency=medium

  * Build-depend on ocaml-dune
  * Bump debhelper compat level to 13
  * Bump Standards-Version to 4.5.0
  * Add Rules-Requires-Root: no

 -- Stéphane Glondu <glondu@debian.org>  Tue, 29 Sep 2020 10:43:03 +0200

ocaml-ssl (0.5.9-1) unstable; urgency=medium

  [ Kyle Robbertze ]
  * Fix watch file url
  * New upstream version 0.5.9
  * Add new uploader
  * Add new build-dep on dune build system
  * Use debhelper-compat
  * Update build rules to use new upstream build system (dune)
  * Bump Standards-Version to 4.4.0 (no change)

  [ Stephane Glondu ]
  * Remove Samuel from Uploaders

 -- Kyle Robbertze <paddatrapper@debian.org>  Mon, 05 Aug 2019 10:44:35 +0200

ocaml-ssl (0.5.5-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Bump Standards-Version to 4.1.4, no changes required
  * Update Vcs-* fields

 -- Mehdi Dogguy <mehdi@debian.org>  Fri, 11 May 2018 13:31:19 +0200

ocaml-ssl (0.5.3-1) unstable; urgency=medium

  * New upstream release
    - remove all patches (applied upstream)
  * Update watch to look at github
  * Bump debhelper compat to 10

 -- Stéphane Glondu <glondu@debian.org>  Tue, 18 Jul 2017 21:46:33 +0200

ocaml-ssl (0.5.2-2) unstable; urgency=medium

  * Team upload.
  * Use accessor functions for X509_STORE_CTX (Closes: #828462).
    - add 0001-Use-accessor-functions-for-X509_STORE_CTX.patch
  * Bump Standards-Version to 3.9.8, no changes required.
  * Update Homepage field.
  * Update Vcs-* fields to use secure and canonical URIs.

 -- Mehdi Dogguy <mehdi@debian.org>  Thu, 03 Nov 2016 00:49:11 +0100

ocaml-ssl (0.5.2-1) unstable; urgency=medium

  * New upstream release
    - Disables SSLv3 related functions when OPENSSL_NO_SSL3 is defined.
  * Remove patch 0001-Remove-SSLv3-support.patch, fixed upstream.

 -- Mehdi Dogguy <mehdi@debian.org>  Sun, 17 Jan 2016 23:56:57 +0100

ocaml-ssl (0.5.1-2) unstable; urgency=medium

  * Remove SSLv3 support
  * Fix license short name in debian/copyright
  * Bump debhelper compat level to 9
  * Bump Standards-Version to 3.9.6

 -- Stéphane Glondu <glondu@debian.org>  Thu, 05 Nov 2015 10:02:08 +0100

ocaml-ssl (0.5.1-1) unstable; urgency=medium

  * New upstream release

 -- Stéphane Glondu <glondu@debian.org>  Wed, 14 Oct 2015 11:23:35 +0200

ocaml-ssl (0.4.7-1) unstable; urgency=medium

  * New upstream release
  * Put debian/copyright in format 1.0
  * Bump Standards-Version to 3.9.5
  * Update Vcs-*

 -- Stéphane Glondu <glondu@debian.org>  Thu, 14 Aug 2014 16:26:36 +0200

ocaml-ssl (0.4.6-3) unstable; urgency=low

  * Upload to unstable

 -- Stéphane Glondu <glondu@debian.org>  Tue, 03 Dec 2013 08:28:14 +0100

ocaml-ssl (0.4.6-2) experimental; urgency=low

  * Compile with OCaml >= 4

 -- Stéphane Glondu <glondu@debian.org>  Thu, 25 Jul 2013 21:41:08 +0200

ocaml-ssl (0.4.6-1) unstable; urgency=low

  * New upstream release
    - SSLv2 support dropped upstream, no more patches

 -- Stéphane Glondu <glondu@debian.org>  Sat, 19 Nov 2011 15:30:29 +0100

ocaml-ssl (0.4.5-1) unstable; urgency=low

  * New upstream release

 -- Stéphane Glondu <glondu@debian.org>  Mon, 18 Apr 2011 22:44:38 +0200

ocaml-ssl (0.4.4-3) unstable; urgency=low

  * Drop SSLv2 constructor from Ssl interface (follows fix to #622144)

 -- Stéphane Glondu <glondu@debian.org>  Mon, 18 Apr 2011 11:14:38 +0200

ocaml-ssl (0.4.4-2) unstable; urgency=low

  * Drop SSLv2 support (Closes: #622144)
  * Switch source package format to 3.0 (quilt)
  * Remove Stefano from Uploaders
  * Bump Standards-Version to 3.9.2 (no changes)

 -- Stéphane Glondu <glondu@debian.org>  Mon, 11 Apr 2011 23:17:43 +0200

ocaml-ssl (0.4.4-1) unstable; urgency=low

  [ Samuel Mimram ]
  * New upstream release.
  * Removed makefile patch, integrated upstream.
  * Switch to dh.
  * Remove dpatch support.

  [ Stéphane Glondu ]
  * Bump Standards-Version to 3.8.4 (no changes)

 -- Stéphane Glondu <glondu@debian.org>  Tue, 02 Mar 2010 23:39:15 +0100

ocaml-ssl (0.4.3-3) unstable; urgency=low

  [ Sylvain Le Gall ]
  * Save doc/ directory before cleaning it

  [ Stéphane Glondu ]
  * Switch packaging to dh-ocaml 0.9
  * Add README.source
  * Update copyright file
  * debian/control:
    - move to section ocaml
    - update my e-mail address
    - update Standards-Version to 3.8.3

 -- Stéphane Glondu <glondu@debian.org>  Mon, 05 Oct 2009 15:40:37 +0200

ocaml-ssl (0.4.3-2) unstable; urgency=low

  * Added makefile.dpatch in order not to link with unix and not to use
    deprecated -custom.

 -- Samuel Mimram <smimram@debian.org>  Wed, 25 Feb 2009 18:00:33 +0100

ocaml-ssl (0.4.3-1) unstable; urgency=low

  * New Upstream Version.

 -- Samuel Mimram <smimram@debian.org>  Wed, 25 Feb 2009 10:43:05 +0100

ocaml-ssl (0.4.2-5) experimental; urgency=low

  * Rebuild against OCaml 3.11
  * debian/control
    - clean up obsolete build-dep on dpkg-dev
    - bump Standards-Version to 3.8.0 (no changes needed)
    - bumpd ocaml build-dep to ensure proper rebuild on experimental
    - add build-dep on dh-ocaml and cdbs
    - add missing ${misc:Depends} dependencies
  * debian/rules: switch packaging to CDBS
  * bump debhelper requirements to 7 (change build-dep and compat
    accordingly)
  * add myself as an Uploader

 -- Stefano Zacchiroli <zack@debian.org>  Sun, 01 Feb 2009 21:20:11 +0100

ocaml-ssl (0.4.2-4) unstable; urgency=low

  [ Stefano Zacchiroli ]
  * fix vcs-svn field to point just above the debian/ dir

  [ Ralf Treinen ]
  * Changed doc-base section to Programming/OCaml

  [ Stephane Glondu ]
  * Switching packaging to git
  * Fix GC-unsafe operations in C stubs (Closes: #500591)
  * Set Maintainer to d-o-m, and Uploaders to Sam and me

 -- Stephane Glondu <steph@glondu.net>  Mon, 29 Sep 2008 19:44:18 +0200

ocaml-ssl (0.4.2-3) unstable; urgency=low

  * Upload to unstable.

 -- Samuel Mimram <smimram@debian.org>  Sat, 08 Sep 2007 00:44:10 +0200

ocaml-ssl (0.4.2-2) experimental; urgency=low

  * Rebuild with OCaml 3.10.

 -- Samuel Mimram <smimram@debian.org>  Fri, 13 Jul 2007 10:54:57 +0200

ocaml-ssl (0.4.2-1) unstable; urgency=low

  * New upstream release.

 -- Samuel Mimram <smimram@debian.org>  Sun, 08 Apr 2007 19:02:54 +0200

ocaml-ssl (0.4.1-1) experimental; urgency=low

  * New upstream release.

 -- Samuel Mimram <smimram@debian.org>  Fri, 16 Mar 2007 18:30:03 +0100

ocaml-ssl (0.4.0-1) unstable; urgency=low

  * New upstream release.
  * Made the package binNMU-safe.

 -- Samuel Mimram <smimram@debian.org>  Mon, 20 Nov 2006 20:55:08 +0100

ocaml-ssl (0.3.1-5) unstable; urgency=low

  * Rebuild with OCaml 3.09.2.
  * Updated standards version to 3.7.2, no changes needed.

 -- Samuel Mimram <smimram@debian.org>  Thu, 18 May 2006 16:45:41 +0000

ocaml-ssl (0.3.1-4) unstable; urgency=low

  * Rebuild with OCaml 3.09.1.

 -- Samuel Mimram <smimram@debian.org>  Sat,  7 Jan 2006 18:25:07 +0100

ocaml-ssl (0.3.1-3) unstable; urgency=low

  * Rebuild with findlib > 1.1-3 (1.1-3 was buggy), closes: #342291.
  * Properly handling OCamlABI stuff.
  * Updated watch file.

 -- Samuel Mimram <smimram@debian.org>  Fri,  9 Dec 2005 00:05:30 +0100

ocaml-ssl (0.3.1-2) unstable; urgency=low

  * Rebuild with OCaml 3.09.0.
  * No longer hardcoding OCaml's ABI in debian/* files.

 -- Samuel Mimram <smimram@debian.org>  Sun, 13 Nov 2005 20:24:13 +0100

ocaml-ssl (0.3.1-1) unstable; urgency=low

  * Initial release, closes: #319287.

 -- Samuel Mimram <smimram@debian.org>  Thu, 21 Jul 2005 01:03:40 +0200
